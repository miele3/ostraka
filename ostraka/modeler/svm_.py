import pandas as pd
from sklearn import svm


def train_model(
    training_data: pd.DataFrame,
    labels: pd.Series,
) -> svm.SVC:
    """
    Train a support vector machine model to give probability estimates based
     all of the features given.

    :param training_data:
        The DataFrame to use in training.
    :param labels:
        A series containing the outcome labels for the training_data.
    :return:
        The SVM classifier.
    """
    clf = svm.SVC(probability=True, class_weight="balanced")
    clf.fit(training_data, labels)

    return clf
