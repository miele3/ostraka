import pandas as pd
import xgboost as xgb


def train_model(
    training_data: pd.DataFrame,
    labels: pd.Series,
    forest_params: dict = None,
) -> xgb.XGBRFClassifier:
    """
    Train a random forest model on all of the features given.

    :param training_data:
        The DataFrame to use in training.
    :param labels:
        A series containing the outcome labels for the training_data.
    :param forest_params:
        A dictionary containing random forest hyper parameter definitions.
    :return:
        The trained random forest.
    """
    positive_count = sum(labels)
    negative_count = len(labels) - positive_count
    params = {
        "objective": "binary:logistic",  # output probability
        "scale_pos_weight": negative_count / positive_count,
    }

    if forest_params:
        params.update(forest_params)

    clf = xgb.XGBRFClassifier(use_label_encoder=False, **params)
    clf.fit(training_data, labels)

    return clf
