import pandas as pd
from sklearn.linear_model import LogisticRegression


def train_model(
    training_data: pd.DataFrame,
    labels: pd.Series,
) -> LogisticRegression:
    """
    Train a logistic regression model on all of the features given.

    :param training_data:
        The DataFrame to use in training.
    :param labels:
        A series containing the outcome labels for the training_data.
    :return:
        The logistic regression classifier.
    """
    clf = LogisticRegression(fit_intercept=True)
    clf.fit(training_data, labels)

    return clf
