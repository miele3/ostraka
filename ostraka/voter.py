import typing

import pandas as pd

from ostraka import configs
from ostraka import utils
from ostraka.modeler import logit_
from ostraka.modeler import random_forest_
from ostraka.modeler import svm_


class FeatureImportances(typing.NamedTuple):
    """
    Structure for storing feature importances and coefficients from
    Random Forest and Logistic Regression, respectively
    """

    random_forest: pd.DataFrame
    logistic_regression: pd.DataFrame


class VotingClassifier:
    """
    Soft voting classifier. Modified from sklearn.ensemble._voting so that
    different classifiers can utilize different features. This is beneficial
    because different classifiers also have different limitations, benefits, and
    downsides. For example, we need to be highly attune to feature overlap
    in regression based techniques and wary of multicollinearity. We've also
    found random forests can invest too heavily in a single feature and we may
    want to exclude that feature, even though the regression handles it fine.
    """

    def __init__(
        self,
        regression_features: list[str] = configs.REGRESSION_FEATURES,
        rf_features: list[str] = configs.RF_FEATURES,
        svm_features: list[str] = configs.SVM_FEATURES,
    ):
        """
        Instantiate VotingClassifier with features for each model (logistic
        regression, random forest, and svm.
        """

        self.regression_features = regression_features
        self.rf_features = rf_features
        self.svm_features = svm_features

    def fit(self, training_data: pd.DataFrame, status: pd.Series) -> "VotingClassifier":
        """Fit Logistic Regression, Random Forest, and SVM models to data"""

        self.regression_features = [
            feature
            for feature in self.regression_features
            if utils.response_check(training_data[feature])
        ]

        log_clf = logit_.train_model(training_data[self.regression_features], status)
        rf_clf = random_forest_.train_model(training_data[self.rf_features], status)
        svm_clf = svm_.train_model(training_data[self.svm_features], status)

        self.classifiers = {
            "log": {"clf": log_clf, "features": self.regression_features},
            "svm": {"clf": svm_clf, "features": self.svm_features},
            "rf": {"clf": rf_clf, "features": self.rf_features},
        }

        return self

    def predict_probas(self, features: pd.DataFrame) -> pd.DataFrame:
        """Predict and return probabilities for each model"""

        probabilities = pd.concat(
            [
                pd.DataFrame(
                    clf["clf"].predict_proba(features[clf["features"]]),
                    columns=[f"prob_not_endorsed_{name}", f"prob_endorsed_{name}"],
                )
                for name, clf in self.classifiers.items()
            ],
            axis=1,
        )
        return probabilities.set_index(features.index)

    def predict_proba(self, features: pd.DataFrame) -> pd.DataFrame:
        """Return average probability from all models"""
        probabilities = self.predict_probas(features)
        not_endorsed = [col for col in probabilities.columns if "not" in col]
        endorsed = [col for col in probabilities.columns if col not in not_endorsed]
        return pd.DataFrame(
            {
                "prob_not_endorsed": probabilities[not_endorsed].mean(axis=1),
                "prob_endorsed": probabilities[endorsed].mean(axis=1),
            },
            index=features.index,
        )

    def get_feature_importance(self) -> FeatureImportances:
        """
        Return information on importance of features in the logistic regression
        and random forest models. A good thing to understand features over time
        and watch for any irregularities. Not available for SVM.
        """

        logistic_regression = pd.DataFrame(
            self.classifiers["log"]["clf"].coef_[0],
            index=self.regression_features,
            columns=["importance"],
        ).sort_values("importance", ascending=False)
        random_forest = pd.DataFrame(
            self.classifiers["rf"]["clf"].feature_importances_,
            index=self.rf_features,
            columns=["importance"],
        ).sort_values("importance", ascending=False)

        return FeatureImportances(
            logistic_regression=logistic_regression, random_forest=random_forest
        )
