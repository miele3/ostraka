"""
Define configurations for the project
"""
# Specify features for each model in Voter
RF_FEATURES = ["feature1", "feature2", "feature3", "feature4"]
REGRESSION_FEATURES = ["feature1", "feature2", "feature3"]
SVM_FEATURES = ["feature1", "feature2", "feature3", "feature4"]
