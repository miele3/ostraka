import pandas as pd


def response_check(feature: pd.Series) -> bool:
    """Check if there's more than one affirmative response for a feature."""
    return len(list(feature.mask(feature > 0, 1).value_counts())) > 1
