import pandas as pd

from ostraka.tests import utils
from ostraka.voter import VotingClassifier

FEATURES = utils.get_features()


def test_voting_classifier():
    """Should fit voting classifier and return probabilities and importances"""
    clf = VotingClassifier().fit(FEATURES, utils.get_labels())

    probabilities = clf.predict_probas(FEATURES)
    probability_mean = clf.predict_proba(FEATURES)
    importances = clf.get_feature_importance()
    rf_importances = importances.random_forest
    regression_importances = importances.logistic_regression

    assert isinstance(probabilities, pd.DataFrame)
    assert len(probabilities.columns) == 6  # return columns for each 3 models
    assert isinstance(probability_mean, pd.DataFrame)
    assert len(probability_mean.columns) == 2  # return columns for agg models
    assert isinstance(rf_importances, pd.DataFrame)
    assert isinstance(regression_importances, pd.DataFrame)
