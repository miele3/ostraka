import pandas as pd

from ostraka import utils

FAKE_DF = pd.DataFrame({"valid": [0, 1, 2], "invalid": [0, 0, 0]})


def test_response_check():
    """Should correctly identify columns with valid ranges of data"""
    assert utils.response_check(FAKE_DF["valid"])
    assert not utils.response_check(FAKE_DF["invalid"])
