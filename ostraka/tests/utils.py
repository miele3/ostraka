import pandas as pd
from numpy import random


def get_features():
    """Get a mock dataframe of features."""
    rows = [
        {
            "id": str(i),
            "feature1": bool(random.randint(0, 1)),
            "feature2": random.uniform(0, 2),
            "feature3": random.randint(0, 3),
            "feature4": random.uniform(0, 1),
        }
        for i in range(200)
    ]
    return pd.DataFrame(rows).set_index(["id"])


def get_labels():
    """Get a mock series of labels"""
    rows = [{"id": str(i), "endorsed": not bool(i % 5)} for i in range(200)]
    df = pd.DataFrame(rows).set_index(["id"])
    return df["endorsed"]
