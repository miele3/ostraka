# ostraka

`ostraka` contains a soft voting classifier combining a random forest, a
logistic regression, and a SVM model. It is modified from 
`sklearn.ensemble._voting` and functions in much the same way, but is capable 
of using different features for the different classifiers. This is beneficial
because different classifiers have different limitations, benefits, and
downsides. For example, we need to be highly attune to feature overlap
in regression based techniques to avoid issues with multicollinearity. With this
Voting Classifier, we can be more parsimonious with our Logistic Regression
features and more liberal with our non-regression based models, where additional
features may add to model complexity and nuance in a beneficial way.

## Instructions
 
Features for each model can be modified in the `configs.py` file or when
instantiating the class. 

```python
from ostraka.voter import VotingClassifier 

full_features = ['feature1', 'feature2', 'feature3', 'feature4']
limited_features = ['feature1', 'feature2', 'feature3']
voter = VotingClassifier(
    regression_features=limited_features, 
    rf_features=full_features, 
    svm_features=full_features
)
```

Similar to sklearn, the voting classifier can then be fit using `.fit()` 
```python
classifier = voter.fit(training_features, outcome_status)
```

Probabilities from the final model can be obtained by using `.predict_proba()`
```python
probabilities: pd.DataFrame = classifier.predict_proba(features_df)
```
A 

Probabilities from each individual model can also be obtained using
`.predict_probas()`
```python
model_probabilities: pd.DataFrame = classifier.predict_probas(features_df)
```

Finally, feature importance and coefficients from the Random Forest and
Logistic Regression, respectively, are also saved and output in a class.
```python3
feature_importances = classifier.get_feature_importance()
print(feature_importances.random_forest)
print(feature_importances.logistic_regression)
```
